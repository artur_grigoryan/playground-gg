# \<playground-gg>

This webcomponent follows the [open-wc](https://github.com/open-wc/open-wc) recommendation.

## Installation
```bash
git clone git@gitlab.com:artur_grigoryan/playground-gg.git
cd playground-gg/
npm i playground-gg
```

## Usage
```html
<script type="module">
  import 'playground-gg/playground-gg.js';
</script>


<playground-gg theme="vs-dark" htmlValue="<div></div>" cssValue="div{}" jsValue="console.log('')"></playground-gg>
```



## Tooling configs

For most of the tools, the configuration is in the `package.json` to reduce the amount of files in your project.

If you customize the configuration a lot, you can consider moving them to individual files.

## Local Demo with `es-dev-server`
```bash
npm start
```
To run a local development server that serves the basic demo located in `demo/index.html`
