/* Name: Playground GG - Author: Artur Grigoryan - Year:2020 */
//Importiert Module von LitElement
import {css, html, LitElement} from 'lit-element';

//Markiert den ersten geladenen Playground-GG
let firstInitElement = 1;

//Erstellt eine  Klasse die von LitElement erbt
export class Playgroundgg extends LitElement {
    //Definiert das aussehen des Playground-GGs
    static get styles() {
        return css`
:host {
    display: block;
    font-family: serif;
    font-family: sans-serif
}
.marker{
  height:24px;
  background-color: #efefef;
  padding-left:10px;
  font-size:16px;
  position: relative;
}
.marker span{
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
}
#playground {
    height: calc(100% - 2px);
    width: calc(100% - 2px);
    overflow: hidden;
    border: 1px solid #000;
}
#header {
    background-color: #fff;
    color: #000;
    width: 100%;
    height: 40px;
    position: relative;
    border-bottom: #000 1px solid;
}
#header-wrap {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
monaco-editor {
    position: absolute;
    height: calc(100% - 24px);
    width: 100%;
    overflow: hidden;
}
.panel-v {
    background: #fff;
    color: #000;
    height: calc(100% - 40px);;
    width: 50%;
    float: left;
}
.panel-v.left {
    border-right: #000 1px solid;
    width: calc(50% - 1px);
}
.panel-v .panel-h {
    height: calc(50% - 24px);
    height: 50%;
    width: 100%;
    resize: none;
    position: relative;
}
.panel-v .panel-h.top {
    border-bottom: #000 1px solid;
}
#result {
    width: 100%;
    height: 100%;
}
iframe {
    border: none;
    background: white;
    height: 100%;
    width: 100%;
}
button {
    background-color: #fff;
    border: 1px solid #000;
    color: #000;
    cursor: pointer;
    transition: all .5s;
    border-radius: 0;
    outline: 0;
    padding: 6px 12px;
    font-family: sans-serif;
    font-size: 14px;
}
button:hover {
    border-radius: 0;
    background-color: #000;
    border: 1px solid #fff;
    color: #fff;
}
.select-wrapper {
    padding:0;
    margin-left: 20px;
    background: transparent !important;
    border: transparent !important;
}
.btn_run {
    margin-left: 20px;
}
.btn_save_local {
    margin-left: 20px;
    left: 100px;
}
.btn_load_local {
    left: 250px;
}
.inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}
.inputfile + label {
    font-family: sans-serif;
    font-size: 14px;
    display: inline-block;
    text-align: center;
    height: 16px;
    background-color: #fff;
    border: 1px solid #000;
    color: #000;
    cursor: pointer;
    transition: all .5s;
    border-radius: 0;
    outline: 0;
    padding: 6px 12px;
}
.inputfile:focus + label,
.inputfile + label:hover {
    border-radius: 0;
    background-color: #000;
    border: 1px solid #fff;
    color: #fff;
}
.inputfile + label {
    cursor: pointer;
}
select {
    height: auto;
    font-family: sans-serif;
    font-size: 14px;
    display: inline-block;
    text-align: center;
    background-color: #fff;
    color: #000;
    cursor: pointer;
    border: 1px solid #000;
    border-image: initial;
    transition: all 0.5s ease 0s;
    border-radius: 0px;
    outline: 0px;
    padding: 5px 12px;
}
#input_load_online {
    width: 60px;
    background-color: #fff;
    color: #000;
    font-family: sans-serif;
    font-size: 14px;
    border: 1px solid #000;
    border-image: initial;
    transition: all 0.5s ease 0s;
    border-radius: 0px;
    outline: 0px;
    padding: 6px 12px;
}
#input_save_online {
    width: 60px;
    background-color: #fff;
    color: #000;
    font-family: sans-serif;
    font-size: 14px;
    border: 1px solid #000;
    border-image: initial;
    transition: all 0.5s ease 0s;
    border-radius: 0px;
    outline: 0px;
    padding: 6px 12px;
}
#btn_save_online {
    margin-left: 20px;
}
.switch {
    cursor: pointer;
    padding: 6px 12px;
    height: 16px;
    font-family: sans-serif;
    font-size: 14px;
    color: #000;
    position: relative;
    display: inline-block;
    width: 85px;
    border: #000 1px solid;
    box-sizing: content-box;
    transition: all .5s;
}
.switch:hover {
    border-radius: 0;
    background-color: #000;
    border: 1px solid #fff;
    color: #fff;
}
.switch::after {
    content: '';
    position: absolute;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    right: 12px;
    transition: all 0.3s;
    top: 8px;
    background-color: rgb(141 141 141);
}
.checkbox:checked + .switch::after {
    background-color: rgb(255, 5, 5);;
}
.checkbox {
    display: none;
}
.vs-dark .switch,
.vs-dark select,
.vs-dark #input_load_online,
.vs-dark #input_save_online,
.vs-dark button,
.vs-dark .inputfile + label,
.vs-dark .panel-v {
    background: #1e1e1e;
}
.vs-dark #header {
    background: #1e1e1e;
    border-bottom: 1px solid #fff;
}
.vs-dark .switch,
.vs-dark select,
.vs-dark #input_save_online,
.vs-dark #input_load_online,
.vs-dark button,
.vs-dark .inputfile + label {
    border: 1px solid #fff;
    color: #fff;
}
.vs-dark .switch:hover,
.vs-dark button:hover,
.vs-dark .inputfile:focus + label,
.vs-dark .inputfile + label:hover {
    background-color: #fff;
    border: 1px solid #1e1e1e;
    color: #1e1e1e
}
.vs-dark .switch:hover span,
.vs-dark button:hover span,
.vs-dark .inputfile:focus + label span,
.vs-dark .inputfile + label:hover span {
    color: #1e1e1e
}
.vs-dark .marker{
  background-color: #0f0f0f;
  color: #fff;
}
.hc-black .marker{
  background-color: #202020;
  color: #fff;
}
#playground.hc-black,
#playground.vs-dark{
    border: 1px solid #fff;
}
.hc-black .switch:hover span,
.hc-black button:hover span,
.hc-black .inputfile:focus + label span,
.hc-black .inputfile + label:hover span {
    color: #1e1e1e
}
.vs-dark input:checked + .slider {
    background-color: #777;
}
.vs-dark input:focus + .slider {
    box-shadow: 0 0 1px #777;
}
.hc-black #input_save_online,
.hc-black #input_load_online,
.vs-dark #input_save_online,
.vs-dark #input_load_online {
    color: #fff;
}
.hc-black .panel-v.left,
.vs-dark .panel-v.left,
.hc-black .panel-v .panel-h.top,
.vs-dark .panel-v .panel-h.top {
    border-color: #fff;
}
.vs-dark span,
.hc-black span {
    color: #fff
}
.vs-dark .checkbox:checked + .switch,
.hc-black .checkbox:checked + .switch {
}
.hc-black .switch,
.hc-black select,
.hc-black #input_load_online,
.hc-black #input_save_online,
.hc-black button,
.hc-black .inputfile + label,
.hc-black .panel-v {
    background: #000000;
}
.hc-black #header {
    background: #000000;
    border-bottom: 1px solid #fff;
}
.hc-black .switch,
.hc-black select,
.hc-black #input_save_online,
.hc-black #input_load_online,
.hc-black button,
.hc-black .inputfile + label {
    border: 1px solid #fff;
    color: #fff;
}
.hc-black .switch:hover,
.hc-black button:hover,
.hc-black .inputfile:focus + label,
.hc-black .inputfile + label:hover {
    background-color: #fff;
    border: 1px solid #000000;
    color: #000000;
}
.hc-black input:checked + .slider {
    background-color: #777;
}
.hc-black input:focus + .slider {
    box-shadow: 0 0 1px #777;
}
.w_980 #input_save_online,
.w_980 #input_load_online {
    border-radius: 5px;
    border-width: 2px !important;
}
.w_980 #header {
    height: 70px;
}
.w_980 .panel-v {
    height: calc(100% - 70px);
}
.w_980 .inputfile + label,
.w_980 #btn_save_online,
.w_980 #select_framework,
.w_980 .select-wrapper,
.w_980 .btn_load_online,
.w_980 .btn_save_local,
.w_980 .btn_run {
    position: relative;
    left: 0
}
.w_980 #select_framework {
    opacity: 0;
}
.w_980 .select-wrapper {
    padding: 0;
}
.w_980 #btn_save_online,
.w_980 .select-wrapper,
.w_980 .btn_save_local,
.w_980 .btn_load_online,
.w_980 .btn_run {
    height: 50px;
    width: 50px;
    border: none !important;
    background: transparent !important;
}
.w_980 .inputfile + label::before,
.w_980 #btn_save_online::before,
.w_980 .btn_save_local::before,
.w_980 .select-wrapper::before,
.w_980 .btn_load_online::before,
.w_980 .btn_run::before {
    content: " ";
    position: absolute;
    height: 50px;
    width: 50px;
    top: 0px;
    left: 0px;
}
.w_980 .inputfile + label:hover,
.w_980 .inputfile + label {
    padding: 0;
    margin: 0;
    width: 50px;
    border: transparent !important;
}
.w_980 .btn_load_online::before {
    background-image: url(/img/icons-artur-load-online-201024-v03.svg);
    background-color: #fff;
}
.w_980.hc-black .btn_load_online::before {
    background-image: url(/img/icons-artur-load-online-201024-v04.svg);
    background-color: #000;
}
.w_980.vs-dark .btn_load_online::before {
    background-image: url(/img/icons-artur-load-online-201024-v04.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 .select-wrapper::before {
    background-image: url(/img/icons-artur-framework-201023-v01.svg);
    background-color: #fff;
}
.w_980.hc-black .select-wrapper::before {
    background-image: url(/img/icons-artur-framework-201023-v02.svg);
    background-color: #000;

}
.w_980.vs-dark .select-wrapper::before {
    background-image: url(/img/icons-artur-framework-201023-v02.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 #btn_save_online::before {
    background-image: url(/img/icons-artur-safe-online-201024-v03.svg);
    background-color: #fff;
}
.w_980.hc-black #btn_save_online::before {
    background-image: url(/img/icons-artur-safe-online-201024-v04.svg);
    background-color: #000;
}
.w_980.vs-dark #btn_save_online::before {
    background-image: url(/img/icons-artur-safe-online-201024-v04.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 .inputfile + label::before {
    background-image: url(/img/icons-artur-save-local-201024-v03.svg);
    background-color: #fff;
}
.w_980.hc-black .inputfile + label::before {
    background-image: url(/img/icons-artur-save-local-201024-v04.svg);
    background-color: #000;
}
.w_980.vs-dark .inputfile + label::before {
    background-image: url(/img/icons-artur-save-local-201024-v04.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 .btn_save_local::before {
    background-image: url(/img/icons-artur-load-local-201024-v03.svg);
    background-color: #fff;
}
.w_980.hc-black .btn_save_local::before {
    background-image: url(/img/icons-artur-load-local-201024-v04.svg);
    background-color: #000;
}
.w_980.vs-dark .btn_save_local::before {
    background-image: url(/img/icons-artur-load-local-201024-v04.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 .btn_run::before {
    background-image: url(/img/icons-artur-run-201023-v01.svg);
    background-color: #fff;
}
.w_980.hc-black .btn_run::before {
    background-image: url(/img/icons-artur-run-201023-v02.svg);
    background-color: #000;
}
.w_980.vs-dark .btn_run::before {
    background-image: url(/img/icons-artur-run-201023-v02.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 .switch::before {
    background-image: url(/img/icons-artur-live-grey-201023-v01.svg);
    background-color: #fff;
}
.w_980.hc-black .switch::before {
    background-image: url(/img/icons-artur-live-grey-201023-v02.svg);
    background-color: #000;
}
.w_980.vs-dark .switch::before {
    background-image: url(/img/icons-artur-live-grey-201023-v02.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 .checkbox:checked + .switch::before {
    background-image: url(/img/icons-artur-live-red-201023-v01.svg);
    background-color: #fff;
}
.w_980.hc-black .checkbox:checked + .switch::before {
    background-image: url(/img/icons-artur-live-red-201023-v02.svg);
    background-color: #000;
}
.w_980.vs-dark .checkbox:checked + .switch::before {
    background-image: url(/img/icons-artur-live-red-201023-v02.svg);
    background-color: rgb(30, 30, 30)
}
.w_980 .switch {
    background: transparent;
    position: relative;
    border: none !important;
    background: transparent !important;
}
.w_980 .btn_run {
    height: 36px;
}
.w_980 .btn_run span,
.w_980 .inputfile + label span,
.w_980 #btn_save_online span,
.w_980 .btn_save_local span,
.w_980 .btn_load_online span,
.w_980 .switch span {
    display: none;
}
.w_980 .switch {
    width: auto;
    width: 50px;
    padding: 11px;
    margin: 0;
    padding: 0;
}
.w_980 .btn_load_online::after,
.w_980 .btn_save_local::after,
.w_980 #btn_save_online::after,
.w_980 .switch::after,
.w_980 .btn_save_local::after {
    content: '_';
}
.w_980 .switch::after {
    position: relative;
    width: auto;
    height: auto;
    border-radius: 0;
    right: unset;
    transition: unset;
    top: 0;
    opacity: 0;
}
.w_980 .inputfile + label::before,
.w_980 .switch::before {
    content: " ";
    position: absolute;
    height: 50px;
    width: 50px;
    top: 0;
    left: 0px;
    width: 50px;
    background-repeat: no-repeat;
}
.w_980 .inputfile + label::before {
    top: -14px !important;
}
.w_980 .switch::before{
    top: -17px;
}
.w_980 #input_save_online,
.w_980 #input_load_online {
    height: 46px;
    margin: 0;
    width: 46px;
    padding: 0;
    text-align: center;
}
.w_580.w_980.switch::before {
    top: -17px;
}
.w_580.w_980#input_save_online,
.w_580.w_980#input_load_online {
    height: 46px;
    margin: 0;
    width: 46px;
    padding: 0;
    text-align: center;
}
.w_580.w_980#select_framework {
    height: 100%;
    width: 50px;
}
.w_580 #btn_save_online {
    margin-left: 10px;
}
.w_580 #header {
    height: 35px;
}
.w_580 .panel-v {
    height: calc(100% - 35px);
}
.w_580 .btn_save_online,
.w_580 .btn_save_local,
.w_580 .btn_run,
.w_580 .select-wrapper {
    margin-left: 10px
}
.w_580 #btn_save_online,
.w_580 .select-wrapper,
.w_580 .btn_save_local,
.w_580 .btn_load_online,
.w_580 .btn_run {
    height: 23px;
    width: 25px;
    border: none !important;
    background: transparent !important;
}
.w_580 .inputfile + label::before,
.w_580 #btn_save_online::before,
.w_580 .btn_save_local::before,
.w_580 .select-wrapper::before,
.w_580 .btn_load_online::before,
.w_580 .btn_run::before {
    top: -1px;
    height: 25px;
    width: 25px;
}
.w_580 .btn_load_online::after,
.w_580 .btn_save_local::after,
.w_580 #btn_save_online::after,
.w_580 .switch::after,
.w_580 .btn_save_local::after {
    content: none;
}
.w_580 .inputfile + label:hover,
.w_580 .inputfile + label {
    width: 25px;
}
.w_580 .switch {
    width: 25px;
}
.w_580 .inputfile + label::before,
.w_580 .switch::before {
    height: 25px;
    width: 25px;
    background-repeat: no-repeat;
}
.w_580 .switch::before {
    top: -2px;
}
.w_580 #input_save_online,
.w_580 #input_load_online {
    height: 25px;
    margin: 0;
    width: 25px;
    padding: 0;
    text-align: center;
}
.w_580 .select-wrapper {
    height: 17px;
}
.w_580 #select_framework {
    position: absolute;
    width: 25px;
}
.w_580 .inputfile + label::before {
    top: -2px !important;
}
.w_580 #input_save_online, .w_580 #input_load_online {
    border-radius: 5px;
    border-width: 1px !important;
}
      `;
    }

    //Holt die definierten Attribute aus der HTML-Datei
    static get properties() {
        return {

            htmlValue: {type: String},
            cssValue: {type: String},
            jsValue: {type: String},
            theme: {type: String},
            width: {type: String},
            height: {type: String},
        };
    }

    //Wird beim initialisieren aufgerufen.
    constructor() {
        //Sorgt dafür, dass .this benutzt werden kann
        super();
        //Defniniert Standartwerte für die Attribute
        this.htmlValue = '';
        this.cssValue = '';
        this.jsValue = '';
        this.theme = 'vs-bright';
        this.width = "100%";
        this.height = "500px";
        //Initialisiert Variablen für die Editoren, das zu ladene Framework und  für den Live-Modus
        this.editor_js;
        this.editor_css;
        this.editor_html;
        this.framework;
        this.live_loaded;
        //Lädt die Funktion zur Breitenverwaltung wenn die Browsergröße geändert wird
        window.addEventListener('resize', this.__handleViewports.bind(this));
    }

    //Wird beim anbinden des Playground-GGs an den DOM ausgeführt
    connectedCallback() {
        //Wenn Attribute height und width leere werte bekommen, werden diese durch die Standardwerte ersetzt
        super.connectedCallback();
        if (!this.height) {
            this.height = "500px";
        }
        if (!this.width) {
            this.width = "100%";
        }
        this.style.width = this.width;
        this.style.height = this.height;

    }

    //Lädt den Monaco Ediotr
    __monacoLoader() {
        const script = document.createElement('script');
        script.onload = this.__loadEditor.bind(this);
        script.src = '../dist/monaco-editor/vs/loader.js';
        return script;
    }

    //Lädt wichtige Funktionen beim erzeugen des Playground-GGs
    __loadEditor() {
        //Wenn Enter im ID-Feld gedrückt wird, wird der Code geladen
        this.shadowRoot.querySelector("#input_load_online").addEventListener('keydown', (event) => {
            if (event.key === 'Enter') {
                this.shadowRoot.querySelector(".btn_load_online").click();
            }
        });
        //Lädt beim ersten Editor Monaco Editor Element
        if (firstInitElement === 1) {
            const script = document.createElement('script');
            script.src = '../src/monaco-editor.js';
            this.shadowRoot.appendChild(script);
            firstInitElement=0;
        }

        //Schreibt die Editoren der Code-Felder und das Framework in Variablen
        this.editor_js = this.shadowRoot.querySelector("#js_editor");
        this.editor_css = this.shadowRoot.querySelector("#css_editor");
        this.editor_html = this.shadowRoot.querySelector("#html_editor");
        this.framework = this.shadowRoot.querySelector("#select_framework");
        //Lädt die Funktion zur Breitenverwaltung zum Start der Applikation
        this.__handleViewports.bind(this)();
        //Speichert this in that für spätere Nutzung
        const that = this;
        //Lädt Funktion zum lokalen Speichern wenn der inhalt des entsprechenden Inputs geändert wird.
        this.shadowRoot.getElementById('loadLocal').addEventListener('change', function selectedFileChanged() {
            that.__loadLocal.bind(this)(that);
        });
    }

    __handleViewports() {
        //Sobald die Browsergöße verändert wird, werden Icons Anzeigen wenn der Playground-GG zu schmal und schrift wenn genug Platz in der Breite vorhanden ist
        let playground = this.shadowRoot.querySelector('#playground');
        playground.classList.remove("w_980");
        playground.classList.remove("w_580");
        if (playground.offsetWidth <= 580) {
            playground.classList.add("w_580");
            playground.classList.add("w_980")
        } else if (playground.offsetWidth <= 980) {
            playground.classList.add("w_980")
        }
    }

    //Funktion zum Lokalen speichern
    __loadLocal(that) {
        //Wenn keine Datei geladen wird, kann abgebrochen werden
        if (this.files.length === 0) {
            console.log('No file selected.');
            return;
        }
        //schreibt die Inhalte der Datei an die richtigen Stellen
        let reader = new FileReader();
        reader.onload = function fileReadCompleted() {
            const regExString_css = new RegExp('(?:###css-start###)((.[\\s\\S]*))(?:###css-end###)');
            that.editor_css.updateValue = regExString_css.exec(reader.result)[1];
            const regExString_html = new RegExp("(?:###html-start###)((.[\\s\\S]*))(?:###html-end###)");
            that.editor_html.updateValue = regExString_html.exec(reader.result)[1];
            const regExString_js = new RegExp("(?:###js-start###)((.[\\s\\S]*))(?:###js-end###)");
            that.editor_js.updateValue = regExString_js.exec(reader.result)[1];
            const regExString_fw = new RegExp("(?:###fw-start###)((.[\\s\\S]*))(?:###fw-end###)");
            try {
                that.framework.value = regExString_fw.exec(reader.result)[1];
                that.__changeFramework();
            } catch (e) {
                that.framework.value = "";
                that.__changeFramework();
            }
        };
        reader.readAsText(this.files[0]);
    };

    //Funktion zum lokalen Speichern
    __saveLocal() {
        //Schreibt die Werte in eine Variable und dann in einen <a>-Tag der einen Download startet
        const element = document.createElement('a');
        const save_value = "###css-start###" + this.editor_css.value + "###css-end###" +
            "###html-start###" + this.editor_html.value + "###html-end###" +
            "###js-start###" + this.editor_js.value + "###js-end###" +
            "###fw-start###" + this.framework.value + "###fw-end###";
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(save_value));
        element.setAttribute('download', "playground.gg");

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

    //Asynchrone Funktion zum Online Laden
    async __loadOnline() {
        //Versucht Daten von der REST-API zu holen und schreibt diese dann in die entsprechenden Felder
        try {
            const input_load_id = this.shadowRoot.querySelector("#input_load_online");
            const response = await fetch("http://pgg-rest.imagine-vs.webseiten.cc/playgrounds/" + input_load_id.value);
            const json = await response.json();
            this.editor_css.updateValue = json.cssCode;
            this.editor_js.updateValue = json.jsCode;
            this.editor_html.updateValue = json.htmlCode;
            if (json.framework) {
                this.framework.value = json.framework;
            } else {
                this.framework.value = "";
            }
            this.__changeFramework();
        } catch (e) {
            console.log("Can’t access response. Blocked by browser?\n" + e);
        }
    }

    //Asynchrone Funktion zum online Speichern
    async __saveOnline() {
        //Versucht Daten von per REST-API auf die Datenbank zu schreiben und schreibt ID in entsprechendes Feld
        try {
            const input_save_id = this.shadowRoot.querySelector("#input_save_online");
            const response = await fetch("http://pgg-rest.imagine-vs.webseiten.cc/playgrounds/", {
                method: 'POST',
                body: 'js=' + encodeURIComponent(this.editor_js.value) + '&html=' + encodeURIComponent(this.editor_html.value) + '&css=' + this.editor_css.value + '&fw=' + this.framework.value,
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                }
            },)
            input_save_id.value = await response.text();
        } catch (e) {
            console.log("Can’t access response. Blocked by browser?\n" + e);
        }
    }

    //Wenn das Framework geändert wird, werden entsprechnde Felder umgeschrieben
    __changeFramework() {
        if (this.framework.value) {
            this.shadowRoot.getElementById("jsMarker").innerHTML = "JavaScript + " + this.framework.options[this.framework.selectedIndex].text;
        } else {
            this.shadowRoot.getElementById("jsMarker").innerHTML = "JavaScript";
        }
        if (this.live_loaded) {
            this.__run();

        }
    }

    //Beim klicken des Run-Buttons
    __run() {
        //Erzeugt Iframe mit Ausgabe
        const framework = this.shadowRoot.querySelector("#select_framework");
        this.shadowRoot.getElementById("result").innerHTML = "";
        const result = this.shadowRoot.querySelector('#result');
        const iframe = document.createElement('iframe');
        const html = "<style>" + this.editor_css.value + "</style>" + this.editor_html.value + framework.value + "<script>" + this.editor_js.value + "</script>";
        result.appendChild(iframe);
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(html);
        iframe.contentWindow.document.close();
    }

    //Führ Run aus wenn Live-Modus aktiviert ist und sich Code oder Framework ändern
    __autorun() {
        const switch_live = this.shadowRoot.querySelector("#switch_live");
        this.__run();

        if (!this.live_loaded) {
            this.editor_html.editor.onDidChangeModelContent(event => {
                if (switch_live.checked) {
                    this.__run();
                }
            });
            this.editor_css.editor.onDidChangeModelContent(event => {
                if (switch_live.checked) {
                    this.__run();
                }
            });
            this.editor_js.editor.onDidChangeModelContent(event => {
                if (switch_live.checked) {
                    this.__run();
                }
            });
        }
        this.live_loaded = 1;
    }

    //Erzeugt HTML-Code
    render() {
        return html`
    <div id="playground" class="${this.theme}">
      <div  id="header">
      <div id="header-wrap">
      <button class="select-wrapper">
        <select id="select_framework" @change=${this.__changeFramework}>
          <option  selected value>-Framework-</option>
          <option value='<script src="../frameworks/jquery-3.5.1.min.js" ></script>'>jQuery</option>
          <option value='<script src="../frameworks/vue.js"></script>'>Vue.js</option>
          <option value='<script src="../frameworks/angular.js"></script>'">Angular</option>
        </select>
      </button>
        <button class="btn_run" @click=${this.__run}><span>Run</span></button>
        <input type="checkbox" id="switch_live" @click=${this.__autorun}  class="checkbox" />
        <label for="switch_live" class="switch"><span>Live Mode</span></label>
        <button class="btn_save_local" @click=${this.__saveLocal}><span>Save local</span></button>
        <input type="file" name="loadLocal" id="loadLocal" class="inputfile" />
        <label for="loadLocal"><span>Load local</span></label>
        <button id="btn_save_online" @click=${this.__saveOnline}><span>Save Online</span></button>
        <input name="id" id="input_save_online" disabled placeholder="ID">
        <button class="btn_load_online" @click=${this.__loadOnline}><span>Load Online</span></button>
        <input name="id" id="input_load_online" placeholder="ID">
      </div>
      </div>
        <div class="panel-v left" >
          <div class="panel-h top">
              <div class="marker"><span>HTML</span></div>
              <monaco-editor id="html_editor" language="html" no-minimap="true" theme="${this.theme}" value="${this.htmlValue}" namespace="/dist/monaco-editor/vs"></monaco-editor>
          </div>
          <div class="panel-h bottom">
              <div class="marker"><span>CSS</span></div>
              <monaco-editor  id="css_editor" language="css" no-minimap="true" theme="${this.theme}" value="${this.cssValue}" namespace="/dist/monaco-editor/vs"></monaco-editor>
          </div>
        </div>
        <div class="panel-v right">
          <div class="panel-h top">
              <div class="marker"><span id="jsMarker">JavaScript</span></div>
              <monaco-editor id="js_editor" language="javascript" no-minimap="true" theme="${this.theme}" value="${this.jsValue}" namespace="/dist/monaco-editor/vs"></monaco-editor>
          </div>
          <div class="panel-h bottom">
            <div id="result">
            </div>
          </div>
        </div>

      ${this.__monacoLoader()}
    </div>
    `;
    }
}